Call this: https://hub.docker.com/r/mikenye/youtube-dl

Download this: https://www.youtube.com/user/GARRETTmitch

ToDo:
* Download only new videos
* Thumbs up video upon sucessful download
* Remove after 14 days
* Add Giantbomb.com to the downloads
* Figure out how often to run.  Probably daily while bandwidth is available.
* Figure out how to make pfsense talk to this so it can utilize available bandwidth more efficiently.
